const Encore = require('@symfony/webpack-encore');
const webpack = require('webpack');

const env = Encore.isProduction() ? 'prod' : 'dev';

// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    .setOutputPath(`public/build/${env}`)
    .setPublicPath(env === 'prod' ? `/build/${env}` : `/build/${env}`)
    .setManifestKeyPrefix('')

    .addEntry('app', './assets/js/app.js')

    .splitEntryChunks()
    .enableSingleRuntimeChunk()

    .cleanupOutputBeforeBuild()

    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(true)

    .configureBabel((config) => {
        config.presets.push('@babel/flow');
        config.plugins.push('@babel/plugin-proposal-class-properties');
    })

    .configureBabelPresetEnv((config) => {
        config.useBuiltIns = 'usage';
        config.corejs = 3;
    })

    .addPlugin(new webpack.DefinePlugin({
        ENV: JSON.stringify(env),
    }))

    .enableSassLoader()
    //.enableTypeScriptLoader()
    .enableReactPreset()
    //.enableIntegrityHashes(Encore.isProduction())
;

module.exports = Encore.getWebpackConfig();
