// @flow

import {OptionsResolver} from "../options/OptionsResolver";
import type {Expression, SearchParameters, ServerSearchParameters, ServerSearchResult} from "../flow/api-types";
import {v4 as uuid} from 'uuid'
import {searchServers} from "../search/search.js";

export function shouldPageParameterReset(oldParameters: SearchParameters, newParameters: SearchParameters): boolean
{
    let reset = false;

    Object.keys(newParameters).forEach((key: string) => {

        if(key === 'page') return;

        const oldValue = oldParameters[key];
        const newValue = newParameters[key];

        if(!compareParameter(oldValue, newValue)){
            reset = true;
        }
    })

    return reset;
}

function compareParameter(oldValue: *, newValue: *): boolean
{
    if(Array.isArray(oldValue) && Array.isArray(newValue)){
        const length = Math.max(oldValue.length, newValue.length);
        for(let i = 0; i < length; i++){
            if(oldValue[i] !== newValue[i]) return false;
        }
        return true;
    }

    return oldValue === newValue;
}

export function normalizeExpression(expression: Expression): Expression
{
    const type = expression.type;

    let normalized: Expression = {type: type};

    if(type === 'and' || type === 'or'){
        normalized.expressions = [...(expression.expressions ?? [])];
        normalized.expressions = normalized.expressions.map(normalizeExpression);
    }

    if(type === 'equate' || type === 'contains'){
        normalized.propertyPath = expression.propertyPath ?? null;
        normalized.value = expression.value ?? null;
    }

    if(type === 'not'){
        normalized.expression = expression.expression ?? null;
        if(normalized.expression === null) normalized.expression = {type: 'equate'};
        normalized.expression = normalizeExpression(normalized.expression);
    }

    normalized.uuid = expression.uuid ?? uuid();

    return normalized;
}

export default class ServerFinder
{

    parametersResolver: OptionsResolver;

    constructor()
    {
        this.initParametersResolver();
    }

    resolveParameters(parameters: {[string]: *}): ServerSearchParameters
    {
        return this.parametersResolver.resolve(parameters);
    }

    initParametersResolver(): void
    {
        const parametersResolver = new OptionsResolver();

        parametersResolver.addArrayDefinition('tags', 'string', [], false);

        parametersResolver.addStringDefinition('name', null, true);
        parametersResolver.addEnumDefinition('sort', 'name', ['name', 'map'], false);
        parametersResolver.addStringDefinition('order', 'asc', false);
        parametersResolver.addNumberDefinition('minTimeAcceleration', null, true);
        parametersResolver.addBooleanDefinition('notPasswordProtected', true, false);
        parametersResolver.addBooleanDefinition('notBattlEyeProtected', false, false);

        parametersResolver.addArrayDefinition('maps', 'string', [], false);

        parametersResolver.addArrayDefinition('includeMods', 'string', [], true);
        parametersResolver.addArrayDefinition('excludeMods', 'string', [], true);
        parametersResolver.addArrayDefinition('requireMods', 'string', [], true);

        parametersResolver.addNumberDefinition('page', 1, false);
        parametersResolver.addNumberDefinition('perPage', 25, false);

        parametersResolver.addObjectDefinition('expression', null, true);
        parametersResolver.addNormalizer('expression', (expression: Expression|null) => {
            if(expression === null) return null;
            return normalizeExpression(expression);
        });

        this.parametersResolver = parametersResolver;
    }

    getDefaultParameters(): ServerSearchParameters
    {
        return this.parametersResolver.getDefaults();
    }

    async search(parameters: ServerSearchParameters): Promise<ServerSearchResult>
    {
        parameters = this.resolveParameters(parameters);
        return searchServers(parameters);
    }

}