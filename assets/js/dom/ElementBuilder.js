// @flow

export function buildElement(tagName, buildCallback: (builder: ElementBuilder) => void): HTMLElement
{
    const builder = new ElementBuilder(tagName);
    buildCallback(builder);
    return builder.element;
}

export default class ElementBuilder
{

    element: HTMLElement;

    constructor(tagName: string) {
        this.element = document.createElement(tagName);
    }

    attribute(name: string, value: string|number|boolean|undefined): ElementBuilder
    {
        if(value === undefined) value = null;
        this.element.setAttribute(name, value);
        return this;
    }

    text(text: string): ElementBuilder
    {
        this.element.textContent = text;
        return this;
    }

    classNames(classNames: string): ElementBuilder
    {
        classNames.split(' ').forEach((className: string) => {
            className = className.trim();
            if(className === '') return;
            this.element.classList.add(className);
        });
        return this;
    }

    on(eventType: string, callback: () => void): ElementBuilder
    {
        this.element.addEventListener(eventType, callback);
        return this;
    }

    innerHtml(html: string): ElementBuilder
    {
        this.element.innerHTML = html;
        return this;
    }

    textChild(text: string): ElementBuilder
    {
        this.element.append(text);
        return this;
    }

    child(tagName, buildCallback: (builder: ElementBuilder) => void): ElementBuilder
    {
        this.element.append(buildElement(tagName, buildCallback));
        return this;
    }

}