export class SimpleDispatcher
{

    listeners = [];

    addListener(callback: Function): void
    {
        this.listeners.push(callback);
    }

    trigger(data: *)
    {
        this.listeners.forEach((callback) => {
            callback(data);
        })
    }

}