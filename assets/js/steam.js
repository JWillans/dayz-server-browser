export function generateSteamModUrl(workshopId: string): string
{
    return `https://steamcommunity.com/sharedfiles/filedetails/?id=${workshopId}`;
}