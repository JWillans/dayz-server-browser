// @flow

export type SearchParameters = {
    page?: number,
    perPage?: number,
}

export type SearchResult = {
    parameters: SearchParameters,
    items: SearchResultItem[],
    page: number,
    perPage: number,
    total: number,
    totalPages: number,
    prevPage: number|null,
    nextPage: number|null,
}

export type SearchResultItem = {
    [string]: *,
}

export type ServerModSearchParameters = SearchParameters & {
    name: string|null,
}

export type ServerMod = {
    name: string,
    steamWorkshopId: number
};

export type ServerModSearchResult = SearchResult & {
    parameters: ServerModSearchParameters,
    items: ServerMod[],
}

export type ServerEndpoint = {
    ip: string,
    port: number,
}

export type RawServer = {
    sponsor: boolean,
    profile: boolean,
    nameOverride: boolean,
    mods: ServerMod[],
    game: string,
    endpoint: ServerEndpoint,
    name: string,
    map: string,
    mission: string,
    players: number,
    maxPlayers: number,
    environment: string,
    password: boolean,
    version: string,
    vac: boolean,
    gamePort: number,
    shard: string,
    battlEye: boolean,
    timeAcceleration: number,
    time: string,
    firstPersonOnly: boolean,
}

export type Server = RawServer & {
    key: string,
}

export type ServerSearchParameters = SearchParameters & {
    tags?: string[]|null,
    name?: string|null,
    maps?: string[]|null,
    minTimeAcceleration?: number|null,
    notPasswordProtected?: boolean,
    notBattlEyeProtected?: boolean,
    includeMods?: string[],
    excludeMods?: string[],
    requireMods?: string[],
    sort?: 'map'|'name'|null,
    order?: 'asc'|'desc',
    expression: Expression|null,
}

export type ServerList = {
    status: number,
    result: Server[]
}

export type ServerSearchResult = SearchResult & {
    items: Server[],
    parameters: ServerSearchParameters,
}

export type ExpressionType = 'or'|'and'|'equate'|'not'|'contains';

export type Expression = AndExpression|OrExpression|NotExpression|EquateExpression|ContainsExpression & {
    uuid: string,
};

export type AndExpression = {
    type: 'and',
    expressions: Expression[],
}

export type OrExpression = {
    type: 'or',
    expressions: Expression[],
}

export type NotExpression = {
    type: 'not',
    expression: Expression,
}

export type EquateExpression = {
    type: 'equate',
    propertyPath: string,
    value: *,
}

export type ContainsExpression = {
    type: 'contains',
    propertyPath: string,
    value: *,
}

export type MapServerCount = {
    map: string,
    count: number,
}