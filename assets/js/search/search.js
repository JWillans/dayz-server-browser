import type {
    MapServerCount,
    SearchParameters,
    SearchResult,
    Server,
    ServerList,
    ServerMod,
    ServerModSearchParameters,
    ServerSearchParameters,
    ServerSearchResult,
} from "../flow/api-types.js";

import {
    Expression, RawServer,
    ServerModSearchResult,
} from "../flow/api-types.js";

import {getProperty} from "dot-prop";

import {SimpleDispatcher} from "../events.js";

const cache = {
    server_list: null,
    maps: null,
    map_server_counts: null,
    server_mods: null,
    regexps: {},
    server_list_loading: null,
}

const serverListDispatcher = new SimpleDispatcher();

function normalizeMap(map: string): string
{
    return map.toLowerCase().trim();
}

function resolveRegExp(value: string): RegExp
{
    if(cache.regexps[value] !== undefined) return cache.regexps[value];
    cache.regexps[value] = new RegExp(value, 'i');
    return cache.regexps[value];
}

const SERVER_LIST_URL = 'https://dayzsalauncher.com/api/v1/launcher/servers/dayz';

export function getServerList(): Promise<ServerList>
{
    return new Promise(async (resolve, reject) => {

        if(cache.server_list !== null){
            resolve(cache.server_list);
            return;
        }

        serverListDispatcher.addListener(resolve);

        if(cache.server_list_loading) return;
        cache.server_list_loading = true;
        const response = await fetch(SERVER_LIST_URL);

        const serverList: ServerList = await response.json();

        updateCacheWithServerList(serverList);

        cache.server_list = serverList;
        serverListDispatcher.trigger(serverList);
    });
}

export function serversMatch(a: Server, b: Server): boolean
{
    if(a.endpoint.ip !== b.endpoint.ip) return false;
    return a.endpoint.port === b.endpoint.port;
}

function findServerIndex(server: Server, servers: Server[]): number|null
{
    for(let i = 0; i < servers.length; i++){
        if(serversMatch(server, servers[i])) return i;
    }
    return null;
}

function updateCacheWithServerList(serverList: ServerList): void
{
    cache.server_list = serverList;
    cache.maps = null;
    cache.server_mods = null;
    cache.map_server_counts = null;
}

export async function updateServer(server: Server): Promise<void>
{
    const serverList = await getServerList();
    const index = findServerIndex(server, serverList.result);
    if(index === null) return;
    serverList.result[index] = server;
    updateCacheWithServerList(serverList);
    serverListDispatcher.trigger(serverList);
}

export async function getServers(): Promise<RawServer[]>
{
    return (await getServerList()).result;
}

function normaliseServers(servers: RawServer[]): Server[]
{
    return servers.map(server => {
        return {
            key: resolveServerKey(server),
            ...server,
        }
    });
}

export async function searchServers(parameters: ServerSearchParameters): Promise<ServerSearchResult>
{
    let servers = await getServers();
    servers = normaliseServers(servers);
    servers = filterServers(servers, parameters);
    servers = sortServers(servers, parameters);
    return createPaginatedSearchResult(servers, parameters);
}

function filterServers(servers: Server[], parameters: ServerSearchParameters): Server[]
{
    servers = filterServersMinTimeAcceleration(servers, parameters);
    servers = filterServersNotPasswordProtected(servers, parameters);
    servers = filterServersNotBattlEyeProtected(servers, parameters);
    servers = filterServersTags(servers, parameters);
    servers = filterServersName(servers, parameters);
    servers = filterServersMaps(servers, parameters);

    servers = filterServersIncludeMods(servers, parameters);
    servers = filterServersExcludeMods(servers, parameters);
    servers = filterServersRequireMods(servers, parameters);

    servers = filterServersExpression(servers, parameters);

    servers = filterServersDuplicates(servers);

    return servers;
}

function filterServersExpression(servers: Server[], parameters: ServerSearchParameters): Server[]
{
    if(parameters.expression === null) return servers;

    return servers.filter((server: Server) => {
        return testExpression(server, parameters.expression);
    });
}

function filterServersDuplicates(servers: Server[]): Server[]
{
    let foundKeys: string[] = [];

    return servers.filter((server: Server) => {
        if(foundKeys.includes(server.key)) return false;
        foundKeys.push(server.key);
        return true;
    });
}

export function resolveServerKey(server: RawServer): string
{
    return `${server.endpoint.ip}:${server.endpoint.port}`;
}

function resolveServerProperty(server: Server, propertyPath: string): *
{
    if(propertyPath === 'server.modNames'){
        return server.mods.map((mod: ServerMod) => {
            return mod.name;
        })
    }

    return getProperty({server: server}, propertyPath);
}

function testExpression(server: Server, expression: Expression): boolean
{
    if(expression.type === 'equate' || expression.type === 'contains'){
        const value = resolveServerProperty(server, expression.propertyPath);

        if(expression.type === 'equate'){
            return `${value}` === `${expression.value}`;
        }

        if(expression.type === 'contains'){

            const regex = resolveRegExp(`${expression.value}`);

            if(Array.isArray(value)){
                return value.filter((entry) => {
                    return `${entry}`.match(regex);
                }).length > 0;
            }

            return `${value}`.match(regex);

        }

    }

    if(expression.type === 'not'){
        return !testExpression(server, expression.expression);
    }

    if(expression.type === 'or'){
        return expression.expressions.filter((childExpression) => {
            return testExpression(server, childExpression);
        }).length > 0;
    }

    if(expression.type === 'and'){
        return expression.expressions.filter((childExpression) => {
            return testExpression(server, childExpression);
        }).length === expression.expressions.length;
    }

    return true;
}

function intersectMods(mods: ServerMod[], modNames: string): ServerMod[]
{
    return mods.filter((mod: ServerMod) => {
        return modNames.includes(mod.name);
    })
}

function filterServersMinTimeAcceleration(servers: Server[], parameters: ServerSearchParameters): Server[]
{
    if(parameters.minTimeAcceleration === null) return servers;

    return servers.filter((server: Server) => {
        return server.timeAcceleration >= parameters.minTimeAcceleration;
    });
}

function filterServersIncludeMods(servers: Server[], parameters: ServerSearchParameters): Server[]
{
    if(parameters.includeMods.length === 0) return servers;

    return servers.filter((server: Server) => {
        return intersectMods(server.mods, parameters.includeMods).length > 0;
    });
}

function filterServersRequireMods(servers: Server[], parameters: ServerSearchParameters): Server[]
{
    if(parameters.requireMods.length === 0) return servers;

    return servers.filter((server: Server) => {
        return intersectMods(server.mods, parameters.requireMods).length === parameters.requireMods.length;
    });
}

function filterServersExcludeMods(servers: Server[], parameters: ServerSearchParameters): Server[]
{
    if(parameters.excludeMods.length === 0) return servers;

    return servers.filter((server: Server) => {
        return intersectMods(server.mods, parameters.excludeMods).length === 0;
    });
}

function filterServersNotBattlEyeProtected(servers: Server[], parameters: ServerSearchParameters): Server[]
{
    if(!parameters.notBattlEyeProtected) return servers;

    return servers.filter((server: Server) => {
        return !server.battlEye;
    })
}

function filterServersNotPasswordProtected(servers: Server[], parameters: ServerSearchParameters): Server[]
{
    if(!parameters.notPasswordProtected) return servers;

    return servers.filter((server: Server) => {
        return !server.password;
    })
}

function filterServersName(servers: Server[], parameters: ServerSearchParameters): Server[]
{
    if(!parameters.name) return servers;

    const regex = resolveRegExp(parameters.name);

    return servers.filter((server: Server) => {
        return server.name.match(regex);
    });

}

function filterServersTags(servers: Server[], parameters: ServerSearchParameters): Server[]
{
    if(parameters.tags.length === 0) return servers;

    const pve = parameters.tags.includes('pve');
    const pvp = parameters.tags.includes('pvp');

    return servers.filter((server: Server) => {
        if(pve && !server.name.match(/PVE/)) return false;
        if(pvp && !server.name.match(/PVP/)) return false;
        return true;
    });
}

function filterServersMaps(servers: Server[], parameters: ServerSearchParameters): Server[]
{
    if(parameters.maps.length === 0) return servers;

    return servers.filter((server: Server) => {
        return parameters.maps.includes(normalizeMap(server.map));
    });
}

function paginate(items: *[], parameters: SearchParameters): *[]
{
    const perPage = parameters.perPage ?? 25;
    const page = parameters.page ?? 1;

    const start = perPage * (page - 1);
    const end = start + perPage;
    return items.slice(start, end);
}

function paginateServers(servers: Server[], parameters: ServerSearchParameters): Server[]
{
    return paginate(servers, parameters);
}

function sortServers(servers: Server[], parameters: ServerSearchParameters): Server[]
{
    const order = parameters.order === 'desc' ? -1 : 1;

    return servers.sort((a: Server, b: Server) => {

        if(parameters.sort === 'map'){
            return a.map > b.map ? order : -order;
        }

        return a.name > b.name ? order : -order;
    });
}

export async function getMaps(): Promise<string[]>
{
    if(cache.maps !== null) return cache.maps;

    const servers = await getServers();

    const maps = servers.reduce((maps: string[], server: Server) => {
        const map = normalizeMap(server.map);
        if(map === '') return maps;
        if(!maps.includes(map)) maps.push(map);
        return maps;
    }, []);

    cache.maps = maps;
    return maps;
}

export async function getMapServerCounts(): Promise<MapServerCount[]>
{
    if(cache.map_server_counts !== null) return cache.map_server_counts;
    const servers = await getServers();

    const counts = servers.reduce((counts: {[string]: number}, server: Server) => {
        const map = normalizeMap(server.map);
        if(map === '') return counts;
        if(counts[map] === undefined) counts[map] = 0;
        counts[map]++;
        return counts;
    }, {});

    cache.map_server_counts = Object.keys(counts).map((map) => {
        return {
            map: map,
            count: counts[map],
        }
    });

    return cache.map_server_counts;
}

export async function getServerMods(): Promise<ServerMod[]>
{
    if(cache.server_mods !== null) return cache.server_mods;

    const servers = await getServers();
    const mods = {};

    servers.forEach((server: Server) => {
        server.mods.forEach((mod: ServerMod) => {
            if(mods[mod.steamWorkshopId] !== undefined) return;
            mods[mod.steamWorkshopId] = mod;
        });
    });

    cache.server_mods = Object.keys(mods).map((id) => {
        return mods[id];
    })

    return cache.server_mods;
}

export async function searchServerMods(parameters: ServerModSearchParameters): Promise<ServerModSearchResult>
{
    let mods = [];

    if(parameters.name){
        mods = await getServerMods();
        let regex = resolveRegExp(parameters.name);
        mods = mods.filter((mod: ServerMod) => {
            return mod.name.match(regex);
        });
    }

    return createPaginatedSearchResult(mods, parameters);
}

function createPaginatedSearchResult(items: *[], parameters: SearchParameters): SearchResult
{
    const total = items.length;
    const totalPages = Math.ceil(total / parameters.perPage);
    const pageItems = paginate(items, parameters);

    return {
        parameters: parameters,
        items: pageItems,
        page: parameters.page,
        perPage: parameters.perPage,
        total: total,
        totalPages: totalPages,
        prevPage: parameters.page > 1 ? parameters.page - 1 : null,
        nextPage: parameters.page < totalPages ? parameters.page + 1 : null,
    }
}