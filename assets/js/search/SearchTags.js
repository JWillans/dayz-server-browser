import type {Expression, SearchParameters, ServerSearchParameters} from "../flow/api-types";

export type SearchTag = {
    key: string,
    label: string,
    unsetParameters: (parameters: ServerSearchParameters) => ServerSearchParameters
}

function countExpressions(expression: Expression): number
{
    let count = 0;

    let children = [];

    if(expression.type === 'or' || expression.type === 'and'){
        children = expression.expressions ?? [];
    }
    else if(expression.type === 'not'){
        children = [expression.expression];
    }
    else{
        count = 1;
    }

    children.forEach((child: Expression) => {
        count += countExpressions(child);
    })

    return count;
}

export function resolveSearchTags(parameters: ServerSearchParameters): SearchTag[]
{

    const maps = parameters.maps ?? [];
    const tags = parameters.tags ?? []
    const name = parameters.name;

    const includeMods = parameters.includeMods ?? [];
    const excludeMods = parameters.excludeMods ?? [];
    const requireMods = parameters.requireMods ?? [];

    const expression = parameters.expression;

    let searchTags = [];

    if(name){
        searchTags.push({
            key: 'name',
            label: `"${name}"`,
            unsetParameters: (parameters: ServerSearchParameters) => {
                return {
                    ...parameters,
                    name: null,
                }
            }
        })
    }

    if(maps.length > 0){

        const label = (
            maps.length <= 3 ?
            maps.join(', ') :
            `${maps.slice(0, 3).join(', ')}...`
        );

        searchTags.push({
            key: 'maps',
            label: label,
            unsetParameters: (parameters: ServerSearchParameters) => {
                return {
                    ...parameters,
                    maps: null,
                }
            }
        })
    }

    tags.forEach((tag: string) => {
        searchTags.push({
            key: `tag_${tag}`,
            label: tag.toUpperCase(),
            unsetParameters: (parameters: ServerSearchParameters) => {
                return {
                    ...parameters,
                    tags: [...(tags.filter(testTag => testTag !== tag))],
                }
            }
        });
    });

    [
        {key: 'includeMods', prefix: 'includes', mods: includeMods},
        {key: 'excludeMods', prefix: 'excludes', mods: excludeMods},
        {key: 'requireMods', prefix: 'requires', mods: requireMods}
    ].forEach((def) => {
        if(def.mods.length === 0) return;
        searchTags.push({
            key: def.key,
            label: `${def.prefix} ${def.mods.length} ${def.mods.length === 1 ? 'mod' : 'mods'}`,
            unsetParameters: (parameters: SearchParameters) => {
                return {
                    ...parameters,
                    [def.key]: null,
                }
            }
        })
    });

    if(expression !== null){
        const exprCount = countExpressions(expression);

        searchTags.push({
            key: 'expression',
            label: `${exprCount} ${exprCount === 1 ? 'expression' : 'expressions'}`,
            unsetParameters: (parameters: SearchParameters) => {
                return {
                    ...parameters,
                    expression: null,
                }
            }
        })
    }

    if(parameters.minTimeAcceleration !== null){
        searchTags.push({
            key: 'minTimeAcceleration',
            label: `${parameters.minTimeAcceleration}×+`,
            unsetParameters: (parameters: SearchParameters) => {
                return {
                    ...parameters,
                    minTimeAcceleration: null,
                }
            }
        })
    }

    return searchTags;

}