import type {SearchParameters} from "../flow/api-types";
import { v4 as uuidv4 } from 'uuid';

const LOCAL_STORAGE_KEY = 'search_presets';

export class SearchPreset {
    id: string;
    name: string;
    parameters: SearchParameters;
}

export function createPreset(name: string, parameters: SearchParameters): SearchPreset
{
    return {
        id: uuidv4(),
        name: name,
        parameters: {...JSON.parse(JSON.stringify(parameters))},
    }
}

export default class PresetManager
{

    load(): SearchPreset[]
    {
        const presetsJson = localStorage.getItem(LOCAL_STORAGE_KEY);
        return presetsJson !== null ? JSON.parse(presetsJson) : [];
    }

    save(presets: SearchPreset[]): void
    {
        localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(presets));
    }

    findIndex(preset: SearchPreset, property: string = 'id', presets: SearchPreset[]|undefined): number
    {
        if(presets === undefined) presets = this.load();
        return presets.findIndex((testPreset: SearchPreset) => {
            return testPreset[property] === preset[property];
        });
    }

    add(preset: SearchPreset): SearchPreset[]
    {
        let presets = this.load();

        const nameIndex = this.findIndex(preset, 'name', presets);
        if(nameIndex >= 0){
            presets[nameIndex] = preset; // Replace preset with same name
        }
        else{
            presets.push(preset); // Add new preset
        }
        this.save(presets);
        return presets;
    }

    remove(preset: SearchPreset): SearchPreset[]
    {
        let presets = this.load();
        const index = this.findIndex(preset, 'id', presets);
        if(index === -1) return presets;
        presets.splice(index, 1);
        this.save(presets);
        return presets;
    }

}