// @flow

// @todo enum type
// @todo number range constraints

function normalizeValue(definition: OptionDefinition, value: *): *
{
    if(value !== null){
        if(definition.type === 'string'){
            value = `${value}`;
        }
       else if(definition.type === 'enum'){
            if(definition.enumValues === null) throw `No enum values defined for ${definition.name}`;
            if(!definition.enumValues.includes(value)) throw `Value ${JSON.stringify(value)} not found in enum values`;
        }
        else if(definition.type === 'number'){
            value = parseFloat(value);
        }
        else if(definition.type === 'boolean'){
            value = !!value;
        }
        else if(definition.type === 'object'){
            if(typeof value !== 'object') value = {};
        }
        else if(definition.type === 'array'){
            if(definition.entryType === null) throw `entryType not defined for array option "${definition.name}"`
            const entryDefinition = new OptionDefinition(`${definition.name}.entry`, definition.entryType);
            entryDefinition.nullable = definition.entryNullable;
            if(!Array.isArray(value)) value = [];
            value = value.map((entryValue) => {
                return normalizeValue(entryDefinition, entryValue);
            })
        }
    }

    if(value === null && !definition.nullable){
        throw `"${definition.name}" is not nullable`;
    }

    definition.normalizers.forEach((normalizer: Normalizer) => {
        value = normalizer(value);
    })

    return value;
}

type OptionDefinitionType = 'string'|'number'|'boolean'|'array'|'enum'|'object';

export class OptionDefinition
{

    name: string;

    type: OptionDefinitionType;

    nullable: boolean = true;

    enumValues: string[]|null = null;

    normalizers: Normalizer[] = [];

    constructor(name: string, type: OptionDefinitionType, configCallback: (definition: OptionDefinition) => void|null = null) {
        this.name = name;
        this.type = type;
        if(configCallback !== null) configCallback(this);
    }

    entryType: OptionDefinitionType = null;

    entryNullable: boolean = false;

    defaultValue: * = null;
}

type Normalizer = (value: *) => *;

export class OptionsResolver
{

    // @todo toQueryString(options: {[name]: *}): string
    // @todo fromQueryString(query: string): {[name]: *}

    definitions: OptionDefinition[] = [];

    forEachDefinition(callback: (definition: OptionDefinition) => void): void
    {
        Object.keys(this.definitions).forEach((name: string) => {
            callback(this.definitions[name]);
        });
    }

    addDefinition(definition: OptionDefinition): void
    {
        this.definitions.push(definition);
    }

    addArrayDefinition(name: string, entryType: OptionDefinitionType, defaultValue: *[]|null = [], entryNullable: boolean = true): void
    {
        const definition = new OptionDefinition(name, 'array');
        definition.entryType = entryType;
        definition.defaultValue = defaultValue ?? [];
        definition.entryNullable = entryNullable;
        this.addDefinition(definition);
    }

    addObjectDefinition(name: string, defaultValue: {[string]: *}|null = null, nullable: boolean = true): void
    {
        const definition = new OptionDefinition(name, 'object');
        definition.defaultValue = defaultValue;
        definition.nullable = nullable;
        this.addDefinition(definition);
    }

    addBooleanDefinition(name: string, defaultValue: boolean|null = false, nullable: boolean = true): void
    {
        const definition = new OptionDefinition(name, 'boolean');
        definition.defaultValue = defaultValue;
        definition.nullable = nullable;
        this.addDefinition(definition);
    }

    addNumberDefinition(name: string, defaultValue: number|null = null, nullable: boolean = true): void
    {
        const definition = new OptionDefinition(name, 'number');
        definition.defaultValue = defaultValue;
        definition.nullable = nullable;
        this.addDefinition(definition);
    }

    addStringDefinition(name: string, defaultValue: number|null = null, nullable: boolean = true): void
    {
        const definition = new OptionDefinition(name, 'string');
        definition.defaultValue = defaultValue;
        definition.nullable = nullable;
        this.addDefinition(definition);
    }

    findDefinition(name: string): OptionDefinition
    {
        const definition = this.definitions.find((definition: OptionDefinition) => {
            return definition.name === name;
        })
        if(definition === undefined) throw `Definition ${name} does not exist`
        return definition;
    }

    addNormalizer(name: string, normalizer: Normalizer): void
    {
        this.findDefinition(name).normalizers.push(normalizer);
    }

    addEnumDefinition(name: string, defaultValue: string|null = null, enumValues: string[], nullable: boolean = true): void
    {
        const definition = new OptionDefinition(name, 'enum');
        definition.defaultValue = defaultValue;
        definition.enumValues = enumValues;
        definition.nullable = nullable;
        this.addDefinition(definition);
    }

    getDefinition(name: string): OptionDefinition
    {
        if(this.definitions[name] === undefined) throw `"${name}" not defined`;
        return this.definitions[name];
    }

    getDefaults(): {[string]: *}
    {
        let defaults = {};
        this.forEachDefinition((definition: OptionDefinition) => {
            try{
                defaults[definition.name] = normalizeValue(definition, definition.defaultValue);
            }
            catch(e)
            {
                console.error(e);
                defaults[definition.name] = definition.defaultValue;
            }

        });
        return defaults;
    }

    resolve(options: {[string]: *})
    {
        let resolvedOptions = {};

        this.forEachDefinition((definition: OptionDefinition) => {
            let value = options[definition.name] ?? definition.defaultValue;
            try{
                value = normalizeValue(definition, value);
            }
            catch(e)
            {
                console.error(e);
                value = definition.defaultValue;
            }

            resolvedOptions[definition.name] = value;
        })

        return resolvedOptions;
    }

}