// @flow

import React from 'react';
import type {Node} from 'react';

type ButtonProps = {
    label: string,
    onClick: () => void|null,
    small: boolean,
    type: 'link'|'submit',
}

type ButtonState = {}

export default class Button extends React.Component<ButtonProps, ButtonState>
{

    static defaultProps = {
        small: false,
        type: 'link',
        onClick: null,
    }

    constructor(props: ButtonProps) {
        super(props);

        this.state = {}
    }

    onClickLink(e: MouseEvent): void
    {
        e.preventDefault();
        if(this.props.onClick !== null) this.props.onClick();
    }

    getClassName(): string
    {
        return `btn btn-secondary border ${this.props.small ? 'rounded-4 btn-sm py-0 px-2' : ''}`;
    }

    renderLink(): Node {
        return (
            <a href="#" className={this.getClassName()} onClick={this.onClickLink.bind(this)}>{this.props.label}</a>
        )
    }

    renderSubmit(): Node {
        return (
            <button type="submit" className={this.getClassName()}>{this.props.label}</button>
        )
    }

    render(): Node
    {
        if(this.props.type === 'submit') return this.renderSubmit();
        return this.renderLink();
    }

}