// @flow

import React from 'react';
import type {Node} from 'react';

type PaginationProps = {
    page: number,
    totalPages: number,
    onChange: (page: number) => void|null,
}

type PaginationState = {}

type PageButtonType = {
    label: string,
    onClick?: () => {},
    disabled?: boolean,
}

export default class Pagination extends React.Component<PaginationProps, PaginationState> {

    constructor(props: PaginationProps) {
        super(props);

        this.state = {}
    }

    createOnClickGotoPage(page: number): (e: MouseEvent) => void
    {
        return (e: MouseEvent) => {
            e.preventDefault();
            if(this.props.onChange === null) return;
            this.props.onChange(page);
        }
    }

    gotoPage(page)
    {
        this.props.onChange(page);
    }

    generateButtons()
    {
        const page = this.props.page;
        const totalPages = this.props.totalPages;

        const nextPage = page < totalPages ? page + 1 : null;
        const prevPage = page > 1 ? page - 1 : null;

        let buttons = [];

        buttons.push({
            label: '⇤',
            onClick: prevPage ? () => {
                this.gotoPage(1);
            } : null,
            disabled: !prevPage,
        });

        buttons.push({
            label: `←`,
            onClick: prevPage ? () => {
                this.gotoPage(prevPage);
            } : null,
            disabled: !prevPage,
        });

        buttons.push({
            label: `Page ${page} of ${totalPages}`,
            type: 'label',
        });

        buttons.push({
            label: '→',
            onClick: nextPage ? () => {
                this.gotoPage(nextPage);
            } : null,
            disabled: !nextPage,
        });

        buttons.push({
            label: `⇥`,
            onClick: nextPage ? () => {
                this.gotoPage(totalPages);
            } : null,
            disabled: !nextPage,
        });

        return buttons;
    }

    render(): Node
    {
        const buttons = this.generateButtons();

        return (
            <div className="d-flex justify-content-end">

                <ul className="pagination">
                    {buttons.map((button: PageButtonType) =>
                        <li className="page-item" key={button.label}>
                            {
                                button.onClick === undefined ?
                                <span className="page-label">{button.label}</span> :
                                <a href="#" className={`page-link${button.disabled ? ' disabled' : ''}`} onClick={(e: MouseEvent) => {
                                    e.preventDefault();
                                    if(button.disabled) return;
                                    button.onClick();
                                }}>{button.label}</a>
                            }
                        </li>
                    )}
                </ul>

            </div>
        );

    }

}