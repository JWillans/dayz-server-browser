// @flow

import React from 'react';
import type {Node} from 'react';

type RemovableTagProps = {
    label: string,
    onClick: () => void,
    onClickRemove: () => void,
    active: boolean,
}

type RemovableTagState = {}

export default class RemovableTag extends React.Component<RemovableTagProps, RemovableTagState>
{

    static defaultProps = {
        active: false,
    }

    constructor(props: RemovableTagProps) {
        super(props);

        this.state = {}
    }

    onClick(e: MouseEvent)
    {
        e.preventDefault();
        this.props.onClick();
    }

    onClickRemove(e: MouseEvent)
    {
        e.preventDefault();
        this.props.onClickRemove();
    }

    render(): Node {
        return (
            <span className={`badge ${this.props.active ? 'bg-primary' : 'bg-secondary'}`}>
                <a href="#" className="text-decoration-none me-2 d-inline-block" onClick={this.onClick.bind(this)}>{this.props.label}</a>
                <a href="#" className="text-decoration-none d-inline-block" onClick={this.onClickRemove.bind(this)}>×</a>
            </span>
        )
    }

}