// @flow

import React from 'react';
import type {Node} from 'react';

type MoreInfoProps = {
    children: Node[],
}

type MoreInfoState = {
    open: boolean,
}

export default class MoreInfo extends React.Component<MoreInfoProps, MoreInfoState> {

    constructor(props: MoreInfoProps) {
        super(props);

        this.state = {
            open: false,
        }
    }

    onClickToggle(e: MouseEvent)
    {
        e.preventDefault();
        this.setState({open: !this.state.open});
    }

    render(): Node {
        return (
            <>
                <a href="#" onClick={this.onClickToggle.bind(this)}><small>{this.state.open ? 'Hide' : 'More info'}</small></a>
                <div className="mt-2">
                    { this.state.open && this.props.children }
                </div>
            </>
        )
    }

}