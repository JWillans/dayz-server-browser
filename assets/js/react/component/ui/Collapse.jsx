// @flow

import React from 'react';
import type {Node} from 'react';

type CollapseProps = {
    id: string,
    children: Node[],
    label: string,
}

type CollapseState = {
    show: boolean,
}

function resolveShownKey(id: string): string
{
    return `collapse.${id}`;
}

function isShown(id: string): boolean
{
    const key = resolveShownKey(id);
    return window.localStorage.getItem(key) === 'shown';
}

function setShown(id: string, shown: boolean): void
{
    const key = resolveShownKey(id);
    return window.localStorage.setItem(key, shown ? 'shown' : 'hidden');
}

export default class Collapse extends React.Component<CollapseProps, CollapseState> {

    constructor(props: CollapseProps) {
        super(props);

        this.state = {
            show: isShown(this.props.id),
        }
    }

    onClickToggle(e: MouseEvent)
    {
        e.preventDefault();
        this.setState({show: !this.state.show}, () => {
            setShown(this.props.id, this.state.show);
        });
    }

    render(): Node {
        return (
            <div className="border rounded pt-2 px-2 mb-2">

                <div className="row mb-2 align-items-center" onClick={this.onClickToggle.bind(this)}>
                    <div className="col-6">
                        <h5 className="mb-0">{this.props.label}</h5>
                    </div>
                    <div className="col-6 text-end">
                        <a className="btn btn-sm btn-outline-secondary" href="#">&#9776;</a>
                    </div>
                </div>

                <div className={`collapse pb-2 ${this.state.show ? 'show' : ''}`}>
                    {this.props.children}
                </div>

            </div>
        )
    }

}