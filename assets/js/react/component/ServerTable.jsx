// @flow

import React from 'react';
import type {Node} from 'react';
import type {Server, ServerMod, ServerSearchParameters} from "../../flow/api-types";
import MoreInfo from "./ui/MoreInfo";
import {generateSteamModUrl} from "../../steam";

type ServerTableColumn = 'name'|'map'|'timeAcceleration'|'ip';

type ServerTableProps = {
    columns: ServerTableColumn[],
    servers: Server[],
    parameters: ServerSearchParameters,
    onChangeParameters: (parameters: ServerSearchParameters) => void|null,
}

export default class ServerTable extends React.Component<ServerTableProps>
{

    static defaultProps = {
        columns: [
            'name',
            'map',
            'timeAcceleration',
            //'ip',
        ],
    }

    excludeMod(mod: string): void
    {
        if(this.props.onChangeParameters === null) return;
        const mods = [...this.props.parameters.excludeMods, mod];
        this.props.onChangeParameters({excludeMods: mods});
    }

    requireMod(mod: string): void
    {
        if(this.props.onChangeParameters === null) return;
        const mods = [...this.props.parameters.requireMods, mod];
        this.props.onChangeParameters({requireMods: mods});
    }

    renderModsTable(mods: ServerMod[]): Node
    {
        return (
            <table className="table table-striped">
                <tbody>
                    {mods.map((mod: ServerMod) =>
                        <tr key={mod.steamWorkshopId}>
                            <td><a href={generateSteamModUrl(mod.steamWorkshopId)} target="_blank">{mod.name}</a></td>
                            <td className="text-end">
                                <a href="#" onClick={(e: MouseEvent) => { e.preventDefault(); this.excludeMod(mod.name); }} className="btn btn-sm btn-outline-secondary">Exclude</a>
                                <a href="#" onClick={(e: MouseEvent) => { e.preventDefault(); this.requireMod(mod.name); }} className="btn btn-sm btn-outline-secondary ms-2">Require</a>
                            </td>
                        </tr>
                    )}
                </tbody>
            </table>
        )
    }

    renderServerRow(server: Server): Node
    {
        const columns: string[] = this.props.columns;

        return (
            <tr key={server.key} className="server-table--server">
                {columns.includes('name') && <td className="server-table--server--name">
                    <span className="me-2">
                        {server.password && <span title="Password Protected" className="pe-2">&#128273;</span> }
                        {server.name}
                    </span>

                    <MoreInfo>

                        <div>
                            <small>{server.endpoint.ip}:{server.endpoint.port}</small><br/>
                            <small>v{server.version}</small>
                        </div>

                        {this.renderModsTable(server.mods)}
                    </MoreInfo>
                </td>}
                {columns.includes('map') && <td className="server-table--server--map">{server.map}</td>}
                {columns.includes('timeAcceleration') && <td className="server-table--server--time-acceleration">{server.timeAcceleration ?? '0'}×</td>}
                {columns.includes('ip') && <td className="server-table--server-ip"><small>{server.endpoint.ip}</small></td>}
            </tr>
        )
    }

    render(): Node
    {
        const columns: string[] = this.props.columns;

        return (
            <table className="table table-striped table-bordered server-table">
                <thead>
                    <tr>
                        {columns.includes('name') && <th>Name</th>}
                        {columns.includes('map') && <th>Map</th>}
                        {columns.includes('timeAcceleration') && <th>Time</th>}
                        {columns.includes('ip') && <th>IP Address</th>}
                    </tr>
                </thead>
                <tbody>
                    {this.props.servers.map(this.renderServerRow.bind(this))}
                </tbody>
            </table>
        )
    }

}