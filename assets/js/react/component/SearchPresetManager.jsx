// @flow

import React from 'react';
import type {Node} from 'react';
import type {SearchParameters} from "../../flow/api-types";
import Collapse from "./ui/Collapse";
import CreateSearchPresetForm from "./form/CreateSearchPresetForm";
import PresetManager, {createPreset, SearchPreset} from "../../search/PresetManager";
import RemovableTag from "./ui/RemovableTag";

const presetManager = new PresetManager();

type SearchPresetManagerProps = {
    parameters: SearchParameters|null,
    onChange: (parameters: SearchParameters) => void,
}

type SearchPresetManagerState = {
    presets: SearchPreset[],
    selected_preset: SearchPreset|null,
}

export default class SearchPresetManager extends React.Component<SearchPresetManagerProps, SearchPresetManagerState> {

    constructor(props: SearchPresetManagerProps) {
        super(props);

        this.state = {
            presets: presetManager.load(),
            selected_preset: null,
        }
    }

    onSubmitCreatePreset(name: string)
    {
        const preset = createPreset(name, this.props.parameters);
        const presets = presetManager.add(preset);
        this.setState({
            presets: presets,
            selected_preset: preset,
        });
    }

    removePreset(preset: SearchPreset)
    {
        const presets = presetManager.remove(preset);
        this.setState({presets: presets});
    }

    selectPreset(preset: SearchPreset)
    {
        this.props.onChange(preset.parameters);
        this.setState({
            selected_preset: preset,
        });
    }

    renderTags(): Node
    {
        const selectedPreset = this.state.selected_preset;

        return (
            <div className="mb-4">
                <span>Presets:</span>
                {this.state.presets.map((preset: SearchPreset) =>
                    <span className="ms-2" key={preset.name}>
                        <RemovableTag
                            active={selectedPreset !== null && selectedPreset.id === preset.id}
                            label={preset.name}
                            onClick={() => { this.selectPreset(preset); }}
                            onClickRemove={() => { this.removePreset(preset); }}
                        />
                    </span>
                )}
            </div>
        )
    }

    render(): Node {

        const selectedPreset = this.state.selected_preset;

        return (
            <>

                <Collapse id="search_presets" label="Presets">

                    {this.renderTags()}

                    <CreateSearchPresetForm
                        onSubmit={this.onSubmitCreatePreset.bind(this)}
                        name={selectedPreset ? selectedPreset.name : null}
                    />

                </Collapse>
            </>
        )
    }

}