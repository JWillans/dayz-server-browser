// @flow

import React from 'react';
import type {Node} from 'react';
import {getServerList} from "../../search/search";
import {generateSteamModUrl} from "../../steam";
import Pagination from "./ui/Pagination";

type ModPopularityProps = {

}

type ModPopularityState = {
    modCounts: ModCount[]|null,
    perPage: number,
    page: number,
}

type ModCount = {
    rank: number,
    name: string,
    steamWorkshopId: number,
    count: number,
}

function autoTruncateModName(name: string): string
{
    if(name.length > 40) return `${name.substring(0, 40)}…`;
    return name;
}

async function getModCounts(): Promise<ModCount[]>
{
    const serverList = await getServerList();

    let modCounts = {};

    serverList.result.forEach(server => {
        server.mods.forEach(mod => {
            if(modCounts[mod.name] === undefined){
                modCounts[mod.name] = {
                    name: mod.name,
                    steamWorkshopId: mod.steamWorkshopId,
                    count: 0,
                }
            }
            modCounts[mod.name].count++;
        })
    })

    modCounts = Object.keys(modCounts).map(key => {
        return modCounts[key];
    });

    modCounts = modCounts.sort((a: ModCount, b: ModCount) => {
        return (a.count > b.count) ? -1 : 1;
    });

    let rankCounter = 0;

    modCounts = modCounts.map(modCount => {
        rankCounter++;
        modCount.rank = rankCounter;
        return modCount;
    })

    return modCounts;
}

export default class ModPopularity extends React.Component<ModPopularityProps, ModPopularityState> {

    state: ModPopularityState;

    constructor(props: ModPopularityProps) {
        super(props);

        this.state = {
            modCounts: null,
            perPage: 50,
            page: 1
        }
    }

    componentDidMount()
    {
        getModCounts().then(modCounts => {
            this.setState({modCounts: modCounts});
        });
    }

    getCurrentPageModCounts(): ModCount[]
    {
        if(this.state.modCounts === null) return [];
        const offset = (this.state.page - 1) * this.state.perPage;
        return this.state.modCounts.slice(offset, offset + this.state.perPage);
    }

    getTotalPages(): number|null
    {
        if(this.state.modCounts === null) return null;
        return Math.ceil(this.state.modCounts.length / this.state.perPage);
    }

    renderPagination(): Node
    {
        return (
            <Pagination page={this.state.page} totalPages={this.getTotalPages()} onChange={page => {
                this.setState({page: page});
            }} />
        )
    }

    render(): Node {

        if (this.state.modCounts === null) {
            return (
                <div className="mod-popularity">
                    Counting mods...
                </div>
            )
        }

        const modCounts = this.getCurrentPageModCounts();

        return (
            <div className="mod-popularity">

                {this.renderPagination()}

                <table className="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Rank</th>
                            <th>Mod Name</th>
                            <th>Server Count</th>
                        </tr>
                    </thead>
                    <tbody>
                        {modCounts.map((modCount: ModCount, index: number) =>
                            <tr key={modCount.name}>
                                <td>#{modCount.rank}</td>
                                <td><a target="_blank" href={generateSteamModUrl(modCount.steamWorkshopId)}>{autoTruncateModName(modCount.name)}</a></td>
                                <td>{modCount.count}</td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>
        );
    }

}