// @flow

import React from 'react';
import type {Node} from 'react';
import type {ServerSearchParameters, ServerSearchResult} from "../../flow/api-types";
import ServerSearchParametersForm from "./form/ServerSearchParametersForm";
import Pagination from "./ui/Pagination";
import ServerTable from "./ServerTable";
import ServerFinder, {shouldPageParameterReset} from "../../finder/ServerFinder";
import SearchPresetManager from "./SearchPresetManager";
import ServerSearchTags from "./ServerSearchTags";
import ModPopularity from "./ModPopularity";

type ServerSearchProps = {
    parameters?: ServerSearchParameters,
}

type ServerSearchState = {
    parameters: ServerSearchParameters,
    loadingServerList: boolean,
    result: ServerSearchResult|null,
    busy: boolean,
}

const finder = new ServerFinder();

const localStorageParametersKey = 'server_search.parameters';

function storeParameters(parameters: ServerSearchParameters): void
{
    window.localStorage.setItem(localStorageParametersKey, JSON.stringify(parameters));
}

function fetchParameters(): ServerSearchParameters|null
{
    const parametersJson = window.localStorage.getItem(localStorageParametersKey);
    if(!parametersJson) return finder.getDefaultParameters();

    let parameters = JSON.parse(parametersJson);
    return finder.resolveParameters(parameters);
}

export default class ServerSearch extends React.Component<ServerSearchProps, ServerSearchState> {

    constructor(props: ServerSearchProps)
    {
        super(props);

        this.state = {
            parameters: this.props.parameters === undefined ? fetchParameters() : finder.resolveParameters(this.props.parameters),
            result: null,
            busy: false,
            loadingServerList: true,
        }
    }

    searchTimeout = null;

    searchDelayed(): void
    {
        clearTimeout(this.searchTimeout);
        this.setState({busy: true}, () => {
            this.searchTimeout = setTimeout(this.search.bind(this), 50);
        });
    }

    async search(): void
    {
        this.setState({busy: true}, () => {

            finder.search(this.state.parameters)
                .then((result: ServerSearchResult) => {
                    this.setState({
                        result: result,
                        busy: false,
                        loadingServerList: false,
                    });
                })
                .catch(console.error)
            ;
        });
    }

    componentDidMount(): void
    {
        this.search();
    }

    setParameters(parameters: ServerSearchParameters, thenSearch: boolean = true, resetUndefinedKeys: boolean = false)
    {
        const resetPage = shouldPageParameterReset(this.state.parameters, parameters);

        if(resetUndefinedKeys){
            parameters = {...finder.getDefaultParameters(), ...parameters};
        }
        else{
            parameters = {...this.state.parameters, ...parameters};
        }


        if(resetPage) parameters.page = 1;
        parameters = finder.resolveParameters(parameters);

        let state = {parameters: parameters};
        if(thenSearch) state.busy = true;

        storeParameters(parameters);
        this.setState(state, () => {
            if(thenSearch) this.searchDelayed();
        })
    }

    renderPagination(): Node|null
    {
        const result: ServerSearchResult|null = this.state.result;

        if(result === null) return null;

        let onChange = this.state.busy ? null : (page: number) => {
            this.setParameters({page: page});
        };

        return (
            <Pagination
                page={result.page}
                totalPages={result.totalPages}
                onChange={onChange}
            />
        )
    }

    renderResultCount(): Node|null
    {
        const result: ServerSearchResult|null = this.state.result;

        if(result === null) return null;

        return (
            <div className="row">
                <div className="col">
                    {result && <p>Found {result.total} servers</p> }
                </div>
                <div className="col text-end">
                    <a href="#" onClick={(e: MouseEvent) => {
                        e.preventDefault();
                        this.clearParameters();
                    }}>&#8634; Clear search parameters</a>
                </div>
            </div>
        )
    }

    clearParameters(): void
    {
        const defaults = finder.getDefaultParameters();
        this.setParameters(defaults);
    }

    render(): Node {

        if (this.state.loadingServerList) {
            return (
                <div className="loading">Fetching server list...</div>
            )
        }

        const result: ServerSearchResult|null = this.state.result;

        const resultStyle = {};
        if(this.state.busy) resultStyle.opacity = 0.5;

        return (
            <>

                <ServerSearchTags parameters={this.state.parameters} onChange={this.setParameters.bind(this)} />

                <SearchPresetManager parameters={this.state.parameters} onChange={this.setParameters.bind(this)} />

                <ServerSearchParametersForm parameters={this.state.parameters} onChange={this.setParameters.bind(this)} />

                <hr/>

                <div style={resultStyle}>
                    {this.renderResultCount()}

                    <div className="mb-3">
                        {this.renderPagination()}
                    </div>

                    <ServerTable
                        servers={result ? result.items : []}
                        parameters={this.state.parameters}
                        onChangeParameters={this.state.busy ? null : this.setParameters.bind(this)}
                    />

                    {this.renderPagination()}
                </div>

            </>
        )
    }

}