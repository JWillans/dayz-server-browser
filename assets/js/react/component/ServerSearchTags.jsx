// @flow

import React from 'react';
import type {Node} from 'react';
import type {ServerSearchParameters} from "../../flow/api-types";
import {resolveSearchTags, SearchTag} from "../../search/SearchTags";

type ServerSearchTagsProps = {
    parameters: ServerSearchParameters,
    onChange: (parameters: ServerSearchParameters) => void,
}

type ServerSearchTagsState = {}

export default class ServerSearchTags extends React.Component<ServerSearchTagsProps, ServerSearchTagsState> {

    constructor(props: ServerSearchTagsProps) {
        super(props);

        this.state = {}
    }

    render(): Node {

        const tags = resolveSearchTags(this.props.parameters)

        return (
            <div className="h4">
                {tags.map((tag: SearchTag) =>
                    <span className="badge bg-secondary me-1" key={tag.key}>
                        {tag.label}
                        <a className="ms-1 text-decoration-none" href="#" onClick={(e: MouseEvent) => {
                            e.preventDefault();
                            console.log(tag);
                            const parameters = tag.unsetParameters(this.props.parameters);
                            this.props.onChange(parameters);
                        }}>×</a>
                    </span>
                )}
            </div>
        )
    }

}