// @flow

import React from 'react';
import type {Node} from 'react';
import ServerSearch from "./ServerSearch";
import ModPopularity from "./ModPopularity";

type AppProps = {}

type AppState = {
    view: string,
}

const NAV_ITEMS = [
    {
        label: 'Server Search',
        view: 'server_search',
    },
    {
        label: 'Mod Popularity',
        view: 'mod_popularity',
    }
]

export default class App extends React.Component<AppProps, AppState> {

    state: AppState;

    constructor(props: AppProps) {
        super(props);

        this.state = {
            view: NAV_ITEMS[0].view,
        }
    }

    renderNav()
    {

        return (
            <ul className="nav nav-pills mb-4">
                {NAV_ITEMS.map(item =>
                    <li className="nav-item" key={item.label}>
                        <a className={`nav-link ${this.state.view === item.view ? 'active' : ''}`} href="#" onClick={e => {
                            e.preventDefault();
                            this.setView(item.view);
                        }}>{item.label}</a>
                    </li>
                )}
            </ul>
        )

    }

    setView(view: string)
    {
        this.setState({view: view});
    }

    renderView(): Node
    {
        if(this.state.view === 'server_search'){
            return <ServerSearch />;
        }

        if(this.state.view === 'mod_popularity'){
            return <ModPopularity />;
        }

        return (
            <div className="alert alert-danger">Invalid view {this.state.view}</div>
        )
    }

    render(): Node {
        return (
            <div className="app">
                {this.renderNav()}
                {this.renderView()}
            </div>
        )
    }

}