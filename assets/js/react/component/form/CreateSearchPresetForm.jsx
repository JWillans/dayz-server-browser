// @flow

import React from 'react';
import type {Node} from 'react';
import TextField from "./field/TextField";
import Button from "../ui/Button";

type CreateSearchPresetFormProps = {
    onSubmit: (name: string) => void,
    name: string|null,
}

type CreateSearchPresetFormState = {
    name: string|null,
}

export default class CreateSearchPresetForm extends React.Component<CreateSearchPresetFormProps, CreateSearchPresetFormState> {

    static defaultProps = {
        name: null,
    }

    constructor(props: CreateSearchPresetFormProps) {
        super(props);

        this.state = {
            name: null,
        }
    }

    componentWillUpdate(nextProps: CreateSearchPresetFormProps)
    {
        if(nextProps.name !== this.props.name){
            this.setState({name: nextProps.name});
        }
    }

    onSubmit(e: FormDataEvent)
    {
        e.preventDefault();
        this.props.onSubmit(this.state.name);
    }

    render(): Node {
        return (
            <form onSubmit={this.onSubmit.bind(this)}>

                <div className="mb-3">
                    <TextField
                        label="Preset Name"
                        value={this.state.name}
                        required={true}
                        onChange={(name: string|null) => { this.setState({name: name}); }}
                    />
                </div>

                <Button type="submit" label="Create/update Preset" />

            </form>
        )
    }

}