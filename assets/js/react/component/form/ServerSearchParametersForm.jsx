// @flow

import React from 'react';
import type {Node} from 'react';
import type {
    AndExpression,
    Expression,
    NotExpression,
    OrExpression,
    ServerSearchParameters
} from "../../../flow/api-types";
import Collapse from "../ui/Collapse";
import MultiCheckboxField from "./field/MultiCheckboxField";
import ServerMapsMultiCheckboxField from "./field/ServerMapsMultiCheckboxField";
import CheckboxField from "./field/CheckboxField";
import NumberField from "./field/NumberField";
import TextField from "./field/TextField";
import ChoiceField from "./field/ChoiceField";
import CollectionField from "./field/CollectionField";
import AutocompleteTextField from "./field/AutocompleteTextField";
import ExpressionField, {
    cloneExpression,
    createContainsExpression,
    createExpression,
    encapsulateExpression
} from "./field/ExpressionField";
import Button from "../ui/Button";
import {resolveSearchTags} from "../../../search/SearchTags";

type ServerSearchParametersFormProps = {
    parameters: ServerSearchParameters,
    onChange: (parameters: ServerSearchParameters) => void,
}

type ServerSearchParametersFormState = {

}

const EXPRESSION_PRESETS = [
    {
        label: "+ Small Items/Large Inventory",
        type: "require",
        mod_names: [
            'DinkyItems',
            'DinkyItemsPlus',
            'Better Smaller items',
            'BetterSmalleritems_2.0',
            'BetterSmalleritems_2.0_Expansion',
            'InventoryPlus',
            'InventoryPlusPLus'
        ],
    },
    {
        label: "+ Kill Rewards",
        type: "require",
        mod_names: [
            'KillReward',
            'KillReward_FIX',
            'RewardSystem',
            'RewardSystemNoKOTH'
        ],
    },
    {
        label: "- Tough Zombies",
        type: "exclude",
        mod_names: [
            'PvZmoD_CustomisableZombies',
            'AJs Creatures',
            'Erapulsar_Zombies',
            'DeadShoreMutants',
            'dbo_creatures',
            'Syberia Project',
        ],
    },
    {
        label: "+ Slow Zombies",
        type: "require",
        mod_names: [
            'SlowZombies',
            'SlowerZeds',
            'WalkingDeadZombies'
        ],
    },
    {
        label: "+ Food",
        type: "require",
        mod_names: [
            'MoreFood',
            'MassFood',
            'MegaFoodPack',
            'EFT Food And Drinks',
            'MAD Food & Drinks',
            'Cookies food and drinks pack',
        ]
    }
]

export default class ServerSearchParametersForm extends React.Component<ServerSearchParametersFormProps, ServerSearchParametersFormState>
{

    static defaultProps = {
    }

    constructor(props: ServerSearchParametersFormProps) {
        super(props);

        this.state = {}
    }

    renderServerGroup(): Node
    {
        const parameters: ServerSearchParameters = this.props.parameters;

        return (
            <Collapse id="server_search.server" label="Server">

                <div className="mb-3">
                    <TextField
                        label="Server Name"
                        value={parameters.name}
                        onChange={(name: string|null) => { this.props.onChange({name: name}) }}
                    />
                </div>

                <div className="mb-3">
                    <NumberField
                        label="Minimum time acceleration"
                        value={parameters.minTimeAcceleration}
                        onChange={(accel: number|null) => { this.props.onChange({minTimeAcceleration: accel}) }}
                    />
                </div>

                <CheckboxField
                    label="Not password protected"
                    checked={this.props.parameters.notPasswordProtected}
                    onChange={(checked: boolean) => { this.props.onChange({notPasswordProtected: checked}) }}
                />

                <CheckboxField
                    label="Not BattlEye protected"
                    checked={this.props.parameters.notBattlEyeProtected}
                    onChange={(checked: boolean) => { this.props.onChange({notBattlEyeProtected: checked}) }}
                />

            </Collapse>
        )
    }

    renderTagsGroup(): Node
    {
        const parameters: ServerSearchParameters = this.props.parameters;

        return (
            <Collapse id="server_search.tags" label="Tags">
                <MultiCheckboxField
                    value={parameters.tags ?? []}
                    onChange={(tags: string[]) => {
                        this.props.onChange({tags: tags});
                    }}
                    choices={[
                        {label: 'PVE', value: 'pve'},
                        {label: 'PVP', value: 'pvp'},
                    ]}
                />
            </Collapse>
        )
    }

    renderMapsGroup(): Node
    {
        const parameters: ServerSearchParameters = this.props.parameters;

        return (
            <Collapse id="server_search.maps" label="Maps">
                <ServerMapsMultiCheckboxField
                    value={parameters.maps ?? []}
                    onChange={(maps: string[]) => {
                        this.props.onChange({maps: maps});
                    }}
                />
            </Collapse>
        )
    }

    renderSortGroup(): Node
    {
        const parameters: ServerSearchParameters = this.props.parameters;

        return (
            <Collapse id="server_search.sort" label="Sort">

                <div className="mb-3">
                    <ChoiceField
                        label="Sort by"
                        value={parameters.sort}
                        placeholder="Default"
                        choices={[
                            {label: 'Name', value: 'name'},
                            {label: 'Map', value: 'map'},
                        ]}
                        onChange={(sort: string|null) => {
                            this.props.onChange({sort: sort});
                        }}
                    />
                </div>

                <div className="mb-3">
                    <ChoiceField
                        label="Order"
                        value={parameters.order}
                        choices={[
                            {label: 'Ascending', value: 'asc'},
                            {label: 'Descending', value: 'desc'},
                        ]}
                        onChange={(order: string|null) => {
                            this.props.onChange({order: order});
                        }}
                    />
                </div>

            </Collapse>
        )
    }

    renderIncludeModsGroup(): Node
    {
        const parameters: ServerSearchParameters = this.props.parameters;

        return (
            <Collapse id="server_search.include_mods" label="Include mods">

                <p><small>(Server must be using <em><strong>at least one</strong></em> of the selected mods)</small></p>

                <CollectionField
                    value={parameters.includeMods}
                    entryType={AutocompleteTextField}
                    entryProps={{sourceName: 'modName'}}
                    addItemLabel="Add mod"
                    onChange={(mods: string[]) => {
                        this.props.onChange({includeMods: mods});
                    }}
                />

            </Collapse>
        )
    }

    renderExcludeModsGroup(): Node
    {
        const parameters: ServerSearchParameters = this.props.parameters;

        return (
            <Collapse id="server_search.exclude_mods" label="Exclude mods">

                <p><small>(Don't show servers using any of the selected mods)</small></p>

                <CollectionField
                    value={parameters.excludeMods}
                    entryType={AutocompleteTextField}
                    entryProps={{sourceName: 'modName'}}
                    addItemLabel="Add mod"
                    onChange={(mods: string[]) => {
                        this.props.onChange({excludeMods: mods});
                    }}
                />

            </Collapse>
        )
    }

    renderRequireModsGroup(): Node
    {
        const parameters: ServerSearchParameters = this.props.parameters;

        return (
            <Collapse id="server_search.require_mods" label="Require mods">

                <p><small>(<em><strong>All</strong></em> of the selected mods must be available on a server)</small></p>

                <CollectionField
                    value={parameters.requireMods}
                    entryType={AutocompleteTextField}
                    entryProps={{sourceName: 'modName'}}
                    addItemLabel="Add mod"
                    onChange={(mods: string[]) => {
                        this.props.onChange({requireMods: mods});
                    }}
                />

            </Collapse>
        )
    }

    enableExpression(): void
    {
        const expression = createExpression('and');
        this.props.onChange({expression: expression});
    }

    renderAdvancedGroup(): Node
    {
        const parameters: ServerSearchParameters = this.props.parameters;

        return (
            <Collapse id="server_search.advanced" label="Advanced">

                {
                    parameters.expression === null ?
                    <Button label="Create expression" onClick={this.enableExpression.bind(this)} /> :
                    <ExpressionField
                        expression={parameters.expression}
                        onChange={(expression: Expression) => {
                            this.props.onChange({expression: expression});
                        }}
                        onRemove={() => {
                            this.props.onChange({expression: null})
                        }}
                    />
                }

                <div className="d-flex gap-2 mt-2">

                    {EXPRESSION_PRESETS.map(preset => {

                        return (
                            <Button
                                key={preset.label}
                                label={preset.label}
                                onClick={() => {

                                    if(preset.type === 'require') {
                                        this.expressionRequireAtLeastOneMod(preset.mod_names);
                                    }
                                    else if(preset.type === 'exclude') {
                                        this.expressionExcludeModNames(preset.mod_names);
                                    }

                                }}
                                small={true}
                            />
                        )

                    })}

                </div>

            </Collapse>
        )
    }

    expressionExcludeModNames(modNames: string[]): void
    {
        const expression: Expression = encapsulateExpression('and', this.props.parameters.expression ?? createExpression('and'));

        const notExpression: NotExpression = createExpression('not');
        expression.expressions.push(notExpression);

        const modOrExpression: OrExpression = createExpression('or');
        notExpression.expression = modOrExpression;

        modNames.forEach(modName => {
            modOrExpression.expressions.push(createContainsExpression('server.modNames', modName));
        });

        this.props.onChange({
            ...this.props.parameters,
            expression: expression,
        });
    }

    expressionRequireAtLeastOneMod(modNames: string[]): void
    {
        const expression: Expression = encapsulateExpression('and', this.props.parameters.expression ?? createExpression('and'));

        const modOrExpression: OrExpression = createExpression('or');

        modNames.forEach(modName => {
            modOrExpression.expressions.push(createContainsExpression('server.modNames', modName));
        });

        expression.expressions.push(modOrExpression);

        this.props.onChange({
            ...this.props.parameters,
            expression: expression,
        });
    }

    render(): Node
    {
        return (
            <div className="row">

                <div className="col-12 col-md-4">
                    {this.renderServerGroup()}
                </div>

                <div className="col-12 col-md-4">
                    {this.renderTagsGroup()}
                </div>

                <div className="col-12 col-md-4">
                    {this.renderSortGroup()}
                </div>

                <div className="col-12">
                    {this.renderMapsGroup()}
                </div>

                <div className="col-12 col-md-4">
                    {this.renderIncludeModsGroup()}
                </div>

                <div className="col-12 col-md-4">
                    {this.renderExcludeModsGroup()}
                </div>

                <div className="col-12 col-md-4">
                    {this.renderRequireModsGroup()}
                </div>

                <div className="col-12">
                    {this.renderAdvancedGroup()}
                </div>

            </div>
        )
    }

}