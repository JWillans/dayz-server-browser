// @flow

import React from 'react';
import type {Node} from 'react';
import type {SearchParameters, SearchResult, SearchResultItem} from "../../../../flow/api-types";
import {searchServerMods} from "../../../../search/search.js";

type SourceDefinition = {
    searchFunction: (parameters: SearchParameters) => Promise<SearchResult>,
    parametersProperty: string,
    resultItemValueProperty: string,
    resultItemIdProperty: string,
}

const sourceDefinitions = {
    modName: {
        searchFunction: searchServerMods,
        parametersProperty: 'name',
        resultItemValueProperty: 'name',
        resultItemIdProperty: 'steamWorkshopId'
    }
}

function findSourceDefinition(name: string): SourceDefinition
{
    if(sourceDefinitions[name] === undefined) throw `Source definition ${name} undefined`;
    return sourceDefinitions[name];
}

type AutocompleteTextFieldProps = {
    label: string,
    value: string|null,
    onChange: (value: string|null) => void,
    sourceName: string,
    searchDelay: number,
}

type AutocompleteTextFieldState = {
    searchValue: string|null,
    sourceDefinition: SourceDefinition,
    result: SearchResult|null,
    mouseOverSuggestion: boolean,
    inputFocus: boolean,
    suggestionsVisible: boolean,
}

export default class AutocompleteTextField extends React.Component<AutocompleteTextFieldProps, AutocompleteTextFieldState>
{

    blurTimeout: number|null = null;

    static defaultProps = {
        searchDelay: 250,
    }

    searchTimeout: TimeoutID|null = null;

    constructor(props: AutocompleteTextFieldProps)
    {
        super(props);

        this.state = {
            searchValue: null,
            sourceDefinition: findSourceDefinition(this.props.sourceName),
            result: null,
            mouseOverSuggestion: false,
            suggestionsVisible: false,
        }
    }

    onChange(e: InputEvent)
    {
        let value = e.target.value;
        if(value === '') value = null;
        clearTimeout(this.searchTimeout);
        this.setState({searchValue: value, result: null}, () => {
            if(this.state.searchValue !== null && this.state.searchValue.length > 1){
                this.search();
            }
        });
    }

    search()
    {
        clearTimeout(this.searchTimeout);

        this.searchTimeout = setTimeout(() => {

            let parameters = {[this.state.sourceDefinition.parametersProperty]: this.state.searchValue};

            this.state.sourceDefinition.searchFunction(parameters)
                .then((result: SearchResult) => {
                    this.setState({result: result}, () => {
                        this.updateSuggestionVisibility();
                    });
                })
                .catch(console.error)
            ;

        }, this.props.searchDelay);
    }

    getSuggestions(): {[string]: string}
    {
        const def: SourceDefinition = this.state.sourceDefinition;

        if(this.state.result === null) return null;

        const items = this.state.result.items;

        let suggestions = {};

        items.forEach((item: SearchResultItem) => {
            const id = `${item[def.resultItemIdProperty]}`;
            suggestions[id] = `${item[def.resultItemValueProperty]}`;
        });

        return suggestions;
    }

    createOnClickSuggestion(suggestion: string): (e: MouseEvent) => void
    {
        return (e: MouseEvent) => {
            e.preventDefault();
            this.setState({searchValue: null, result: null}, () => {
                this.props.onChange(suggestion);
            });
        }
    }

    renderSuggestions(): Node|null
    {
        const suggestions = this.getSuggestions();
        if(!this.state.suggestionsVisible || !suggestions) return null;

        return (
            <div
                className="position-relative z-1000"
                onMouseEnter={this.onMouseEnterSuggestions.bind(this)}
                onMouseLeave={this.onMouseLeaveSuggestions.bind(this)}
            >
                <div className="btn-group-vertical mt-2 position-absolute">
                    {Object.keys(suggestions).map((key: string) =>
                        <a key={key} href="#" className="btn btn-secondary" onClick={this.createOnClickSuggestion(suggestions[key])}>{suggestions[key]}</a>
                    )}
                </div>
            </div>
        )
    }

    onInputFocus(e: InputEvent): void
    {
        this.setState({inputFocus: true}, () => {
            this.updateSuggestionVisibility();
        });
    }

    onInputBlur(e: InputEvent)
    {
        this.setState({inputFocus: false}, () => {
            this.updateSuggestionVisibility();
        });
    }

    onMouseEnterSuggestions(e: MouseEvent): void
    {
        this.setState({mouseOverSuggestion: true}, () => {
            this.updateSuggestionVisibility();
        });
    }

    onMouseLeaveSuggestions(e: MouseEvent): void
    {
        this.setState({mouseLeaveSuggestion: true}, () => {
            this.updateSuggestionVisibility();
        });
    }

    updateSuggestionVisibility(): void
    {
        const visible = this.state.searchValue && (this.state.mouseOverSuggestion || this.state.inputFocus);
        if(this.state.suggestionsVisible === visible) return;
        this.setState({suggestionsVisible: visible});
    }

    render(): Node
    {
        return (
            <>
                { this.props.label && <label className="form-label">{this.props.label}</label> }
                <input
                    onFocus={this.onInputFocus.bind(this)}
                    onBlur={this.onInputBlur.bind(this)}
                    autoComplete="off"
                    type="text"
                    value={this.state.searchValue ?? (this.props.value || '')}
                    className="form-control"
                    onChange={this.onChange.bind(this)}
                />
                {this.renderSuggestions()}
            </>
        )
    }

}