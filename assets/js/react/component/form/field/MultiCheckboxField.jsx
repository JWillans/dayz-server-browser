// @flow

import React from 'react';
import type {Node} from 'react';
import CheckboxField from "./CheckboxField";

export type Choice = {
    key?: string,
    label: string,
    value: string,
}

type MultiCheckboxFieldProps = {
    value: string[],
    onChange: (value: string[]) => void,
    choices: Choice[],
}

type MultiCheckboxFieldState = {}

function batchChoices(choices: Choice[], size: number): Choice[][]
{
    let batches = [];
    let index = -1;
    for(let i = 0; i < choices.length; i++){
        if(i % size === 0){
            index++;
            batches.push([]);
        }
        batches[index].push(choices[i]);
    }
    return batches;
}

function hashChoice(choice: Choice): string
{
    if(choice.key !== undefined) return choice.key;
    return `_${choice.label}_${choice.value}`;
}

export default class MultiCheckboxField extends React.Component<MultiCheckboxFieldProps, MultiCheckboxFieldState> {

    constructor(props: MultiCheckboxFieldProps) {
        super(props);

        this.state = {}
    }

    invertSelection()
    {
        let value = [];

        this.props.choices.forEach((choice: Choice) => {
            if(this.props.value.includes(choice.value)) return;
            value.push(choice.value);
        })

        this.props.onChange(value);
    }

    selectAll()
    {
        const value = this.props.choices.map((choice: Choice) => {
            return choice.value;
        })
        this.props.onChange(value);
    }

    selectNone()
    {
        this.props.onChange([]);
    }

    renderControls()
    {
        return (
            <div className="mb-2">
                <span>Select: </span>
                <a href="#" onClick={(e: MouseEvent) => { e.preventDefault(); this.invertSelection(); }}>Invert</a>
                <span> | </span>
                <a href="#" onClick={(e: MouseEvent) => { e.preventDefault(); this.selectAll();}}>All</a>
                <span> | </span>
                <a href="#" onClick={(e: MouseEvent) => { e.preventDefault(); this.selectNone(); }}>None</a>
            </div>
        )
    }

    addChoiceToValue(choice: Choice): void
    {
        let value = [...this.props.value];
        if(value.includes(choice.value)) return;
        value.push(choice.value);
        this.props.onChange(value);
    }

    removeChoiceFromValue(choice: Choice): void
    {
        let value = [...this.props.value];
        const index = value.indexOf(choice.value);
        if(index === -1) return;
        value.splice(index, 1);
        this.props.onChange(value);
    }

    renderCheckboxes()
    {
        const choices = this.props.choices;

        const value = this.props.value;

        const numCols = choices.length > 15 ? 3 : 1;
        const batchSize = Math.ceil(choices.length / numCols);

        const choiceBatches = batchChoices(choices, batchSize);

        return (
            <div className="row">
                {choiceBatches.map((batch: Choice[], index: number) =>
                    <div className="col-12 col-md" key={`choice_col_${index}`}>
                        {batch.map((choice: Choice) =>
                            <CheckboxField
                                label={choice.label}
                                checked={value.includes(choice.value)}
                                onChange={(checked: boolean) => {
                                    if(checked) this.addChoiceToValue(choice);
                                    else this.removeChoiceFromValue(choice);
                                }}
                                key={hashChoice(choice)}
                            />
                        )}
                    </div>
                )}
            </div>
        )
    }

    render(): Node {
        return (
            <>
                {this.renderControls()}
                {this.renderCheckboxes()}
            </>
        )
    }

}