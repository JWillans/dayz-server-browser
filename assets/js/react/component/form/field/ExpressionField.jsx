// @flow

import React from 'react';
import type {Node} from 'react';
import type {
    AndExpression,
    ContainsExpression,
    Expression,
    ExpressionType,
    OrExpression
} from "../../../../flow/api-types";
import ChoiceField from "./ChoiceField";
import Button from "../../ui/Button";
import TextField from "./TextField";
import AutocompleteTextField from "./AutocompleteTextField";
import {normalizeExpression} from "../../../../finder/ServerFinder";
import {v4 as uuid} from "uuid";

export function createExpression(type: ExpressionType): Expression
{
    return normalizeExpression({type: type});
}

export function createContainsExpression(propertyPath: string, value: any): ContainsExpression
{
    let expression: ContainsExpression = createExpression('contains');
    expression.propertyPath = propertyPath;
    expression.value = value;
    return expression;
}

export function cloneExpression(expression: Expression): Expression
{
    expression = {
        ...expression,
        uuid: uuid(),
    };

    if(expression.type === 'and' || expression.type === 'or'){
        expression.expressions.map((child: Expression) => {
            return cloneExpression(child);
        })
    }
    else if(expression.type === 'not'){
        if(expression.expression){
            expression.expression = cloneExpression(expression.expression);
        }
    }

    return expression;
}

export function encapsulateExpression(parentType: ExpressionType, expression: Expression): Expression
{
    if(expression.type === parentType) return expression;

    if(parentType !== 'or' && parentType !== 'and') throw 'Parent type must be `and` or `or`';

    let parent: AndExpression|OrExpression = createExpression(parentType);
    parent.expressions.push(expression);

    return parent;
}

type ExpressionFieldProps = {
    expression: Expression|null,
    container: boolean,
    onChange: (expression: Expression) => void,
    onRemove: () => void|null,
    depth: number,
}

type ExpressionFieldState = {}

export default class ExpressionField extends React.Component<ExpressionFieldProps, ExpressionFieldState>
{

    static defaultProps = {
        depth: 0,
        onRemove: null,
        container: true,
    }

    constructor(props: ExpressionFieldProps)
    {
        super(props);

        this.state = {}
    }

    changeType(type: ExpressionType)
    {
        this.changeProperty('type', type);
    }

    changeProperty(name: string, value: *)
    {
        const newExpression = normalizeExpression({...this.props.expression, [name]: value});
        this.props.onChange(newExpression);
    }

    renderTypeField()
    {
        return (
            <div className={`col-12 ${this.props.expression.type === 'not' ? 'col-md-2' : 'col-md-4'}`}>
                <ChoiceField
                    onChange={this.changeType.bind(this)}
                    choices={[
                        {label: 'And', value: 'and'},
                        {label: 'Or', value: 'or'},
                        {label: 'Not', value: 'not'},
                        {label: 'Equals', value: 'equate'},
                        {label: 'Contains', value: 'contains'},
                    ]}
                    value={this.props.expression.type}
                />
            </div>
        )
    }

    addChildExpression(): void
    {
        let expressions = [...this.props.expression.expressions];

        let childExpression: Expression;

        if(expressions.length > 0){
            childExpression = {...cloneExpression(expressions[expressions.length - 1])}
        }
        else{
            childExpression = createExpression('equate');
        }

        expressions.push(childExpression);
        this.changeProperty('expressions', expressions);
    }

    removeChildExpression(index: number): void
    {
        let expressions = [...this.props.expression.expressions];
        expressions.splice(index, 1);
        this.changeProperty('expressions', expressions);
    }

    updateChildExpression(index: number, expression: Expression): void
    {
        let expressions = [...this.props.expression.expressions];
        expressions[index] = expression;
        this.changeProperty('expressions', expressions);
    }

    renderPropertyPathField(): Node|null
    {
        const expression: Expression = this.props.expression;
        if(!['contains', 'equate'].includes(expression.type)) return null;

        return (
            <div className="col-12 col-md-4">
                <ChoiceField
                    placeholder=""
                    onChange={(propertyPath: string) => {
                        this.changeProperty('propertyPath', propertyPath);
                    }}
                    choices={[
                        {label: 'Server > Name', value: 'server.name'},
                        {label: 'Server > Map', value: 'server.map'},
                        {label: 'Server > Mod Names', value: 'server.modNames'},
                        {label: 'Server > Version', value: 'server.version'},
                        {label: 'Server > Time Acceleration', value: 'server.timeAcceleration'}

                    ]}
                    value={expression.propertyPath ?? null}
                />
            </div>
        )
    }

    renderValueField(): Node|null
    {
        const expression: Expression = this.props.expression;
        if(!['contains', 'equate'].includes(expression.type)) return null;

        if(expression.propertyPath === 'server.modNames'){
            return (
                <div className="col-12 col-md-4">
                    <AutocompleteTextField
                        sourceName="modName"
                        onChange={(value: string) => {
                            this.changeProperty('value', value);
                        }}
                        value={expression.value ?? null}
                    />
                </div>
            )
        }

        return (
            <div className="col-12 col-md-4">
                <TextField
                    onChange={(value: string) => {
                        this.changeProperty('value', value);
                    }}
                    value={expression.value ?? null}
                />
            </div>
        )
    }

    renderChildExpressionsFields(): Node|null
    {
        const expression: Expression = this.props.expression;
        if(!['and', 'or'].includes(expression.type)) return null;

        const children = expression.expressions;

        return (
            <div className="col-12">
                <div className="mt-2">
                    {children.map((child: Expression, index: number) =>
                        <div key={child.uuid}>
                            {index > 0 && <div className="mt-2 ms-5">{expression.type}</div> }
                            <div className="mt-2 ms-5">
                                <ExpressionField
                                    expression={child}
                                    depth={this.props.depth + 1}
                                    onChange={(updatedChild: Expression) => {
                                        this.updateChildExpression(index, updatedChild);
                                    }}
                                    onRemove={() => {
                                        this.removeChildExpression(index);
                                    }}
                                />
                            </div>
                        </div>
                    )}
                </div>
                <div className="mt-2">
                    <Button label="Add item" small={true} onClick={this.addChildExpression.bind(this)} />
                </div>
            </div>
        )
    }

    renderChildExpressionField(): Node|null
    {
        const expression: Expression = this.props.expression;
        if(!['not'].includes(expression.type)) return null;

        const child = expression.expression;
        if(child === null) return null;

        return (
            <div className="col-10">
                <ExpressionField
                    expression={child}
                    depth={this.props.depth + 1}
                    container={false}
                    onChange={(updatedChild: Expression) => {
                        this.changeProperty('expression', updatedChild);
                    }}
                    onRemove={null}
                />
            </div>
        )
    }

    renderInner(): Node
    {
        return (
            <div className={`position-relative ${this.props.depth > 0 ? 'pe-5' : ''}`}>

                {this.props.onRemove !== null && <div className="position-absolute top-0 end-0" style={{zIndex: 1000}}>
                    <Button label={this.props.depth === 0 ? 'Remove' : '×'} onClick={this.props.onRemove} small={true} />
                </div>}

                <div className="row">
                    { this.renderPropertyPathField() }
                    { this.renderTypeField() }
                    { this.renderValueField() }
                    { this.renderChildExpressionField() }
                    { this.renderChildExpressionsFields() }
                </div>

            </div>
        )
    }

    render(): Node {

        if(!this.props.container) return this.renderInner();

        return (
            <div className="bg-light bg-opacity-25 rounded p-2">
                {this.renderInner()}
            </div>
        )
    }

}