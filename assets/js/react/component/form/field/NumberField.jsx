// @flow

import React from 'react';
import type {Node} from 'react';

type NumberFieldProps = {
    label: string|null,
    value: number|null,
    onChange: (value: number|null) => void,
}

type NumberFieldState = {}

export default class NumberField extends React.Component<NumberFieldProps, NumberFieldState> {

    constructor(props: NumberFieldProps) {
        super(props);

        this.state = {}
    }

    onChange(e: InputEvent): void
    {
        let value = e.target.value;

        if(value === '') value = null;
        else value = parseInt(value);

        if(value === 0) value = null;

        this.props.onChange(value);
    }

    render(): Node {
        return (
            <>
                { this.props.label && <label className="form-label">{this.props.label}</label> }
                <input type="number" min={0} className="form-control" value={this.props.value ?? ''} onChange={this.onChange.bind(this)} />
            </>
        )
    }

}