// @flow

import React from 'react';
import type {Node} from 'react';

type TextFieldProps = {
    label: string|null,
    value: string|null,
    onChange: (value: string|null) => void,
    required: boolean,
}

type TextFieldState = {}

export default class TextField extends React.Component<TextFieldProps, TextFieldState> {

    static defaultProps = {
        required: false,
    }

    constructor(props: TextFieldProps) {
        super(props);

        this.state = {}
    }

    onChange(e: InputEvent): void
    {
        let value = e.target.value;

        if(value === '') value = null;
        else value = `${value}`;

        this.props.onChange(value);
    }

    render(): Node {
        return (
            <>
                { this.props.label && <label className="form-label">{this.props.label}</label> }
                <input
                    type="text"
                    className="form-control"
                    value={this.props.value ?? ''}
                    onChange={this.onChange.bind(this)}
                    required={this.props.required}
                />
            </>
        )
    }

}