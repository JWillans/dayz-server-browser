// @flow

import React from 'react';
import type {Node} from 'react';

type Choice = {
    label: string,
    value: string|null,
}

type ChoiceFieldProps = {
    label: string|null,
    placeholder: string|null,
    value: string|null,
    choices: Choice[],
    onChange: (value: string|null) => void,
}

type ChoiceFieldState = {}

export default class ChoiceField extends React.Component<ChoiceFieldProps, ChoiceFieldState> {

    static defaultProps = {
        placeholder: null,
        label: null,
    }

    constructor(props: ChoiceFieldProps) {
        super(props);

        this.state = {}
    }

    onChange(e: InputEvent): void
    {
        let value = e.target.value;
        if(value === '') value = null;
        else value = `${value}`;
        this.props.onChange(value);
    }

    render(): Node {
        return (
            <>
                {this.props.label && <label className="form-label required">{this.props.label}</label>}
                <select className="form-select" onChange={this.onChange.bind(this)} value={this.props.value ?? ''}>
                    {this.props.placeholder !== null && <option value="">{this.props.placeholder}</option>}
                    {this.props.choices.map((choice: Choice) =>
                        <option value={choice.value ?? ''} key={choice.label}>{choice.label}</option>
                    )}
                </select>
            </>
        )
    }

}