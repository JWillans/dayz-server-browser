// @flow

import React from 'react';
import type {Node} from 'react';
import {getMapServerCounts} from "../../../../search/search.js";
import MultiCheckboxField from "./MultiCheckboxField";
import type {Choice} from "./MultiCheckboxField";
import type {MapServerCount} from "../../../../flow/api-types";
import ChoiceField from "./ChoiceField";

type ServerMapsMultiCheckboxFieldProps = {
    value: string[],
    onChange: (value: string[]) => void,
}

type ServerMapsMultiCheckboxFieldState = {
    sort: 'map'|'count',
    counts: MapServerCount[]|null,
}

export default class ServerMapsMultiCheckboxField extends React.Component<ServerMapsMultiCheckboxFieldProps, ServerMapsMultiCheckboxFieldState> {

    constructor(props: ServerMapsMultiCheckboxFieldProps) {
        super(props);

        this.state = {
            sort: 'map',
            counts: null,
        }
    }

    getChoices()
    {
        let counts = this.state.counts;

        if(counts === null) return [];

        if(this.state.sort === 'map'){
            counts = counts.sort((a: MapServerCount, b: MapServerCount) => {
                if(a.map === b.map) return 0;
                return a.map > b.map ? 1 : -1;
            });
        }
        else if(this.state.sort === 'count'){
            counts = counts.sort((a: MapServerCount, b: MapServerCount) => {
                if(a.count === b.count) return 0;
                return a.count > b.count ? 1 : -1;
            });
        }

        return counts.map((count: MapServerCount) => {
            return {
                key: count.map,
                label: <>{count.map} <span className="badge bg-secondary rounded-pill">{count.count}</span></>,
                value: count.map,
            }
        });
    }

    loadMaps()
    {
        getMapServerCounts()
            .then((counts: MapServerCount[]) => {
                this.setState({counts: counts});
            })
        ;
    }

    componentDidMount(): void
    {
        this.loadMaps();
    }

    changeSort(sort: string)
    {
        this.setState({sort: sort});
    }

    renderSort()
    {

        const choices = [
            {label: 'Map Name', value: 'map'},
            {label: 'Count', value: 'count'},
        ]

        return (
            <div className="mb-2">
                <ChoiceField
                    label="Sort by"
                    onChange={this.changeSort.bind(this)}
                    choices={choices}
                    value={this.state.sort}
                />
            </div>
        )
    }

    render(): Node {
        return (
            <div>

                {this.renderSort()}

                <MultiCheckboxField
                    value={this.props.value}
                    onChange={this.props.onChange}
                    choices={this.getChoices()}
                />
            </div>

        )
    }

}