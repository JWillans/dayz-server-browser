// @flow

import React from 'react';
import type {Node} from 'react';

type CheckboxFieldProps = {
    label: string,
    checked: boolean,
    onChange: (checked: boolean) => void,
}

type CheckboxFieldState = {}

export default class CheckboxField extends React.Component<CheckboxFieldProps, CheckboxFieldState>
{

    static defaultProps = {
        key: null,
    }

    constructor(props: CheckboxFieldProps) {
        super(props);

        this.state = {}
    }

    onChange(e: InputEvent)
    {
        const checked = !!e.target.checked;
        this.props.onChange(checked);
    }

    render(): Node {
        return (
            <div className="form-check">
                <input type="checkbox" className="form-check-input" checked={this.props.checked} onChange={this.onChange.bind(this)}/>
                <label className="form-check-label">{this.props.label}</label>
            </div>
        )
    }

}