// @flow

import React from 'react';
import {Component} from 'react';
import type {Node} from 'react';

type CollectionFieldProps = {
    label: string|null,
    value: *[],
    entryType: Component,
    entryProps: {[string]: *},
    onChange: (value: *[]) => void,
    addItemLabel: string,
}

type CollectionFieldState = {}

export default class CollectionField extends React.Component<CollectionFieldProps, CollectionFieldState>
{

    static defaultProps = {
        label: null,
        entryProps: {},
        addItemLabel: 'Add item'
    }

    constructor(props: CollectionFieldProps) {
        super(props);

        this.state = {}
    }

    onClickAddEntry(e: MouseEvent)
    {
        e.preventDefault();
        this.addEntry();
    }

    addEntry(): void
    {
        const value = [...this.props.value, null];
        this.props.onChange(value);
    }

    removeEntry(index: number): void
    {
        let value = [...this.props.value];
        value.splice(index, 1);
        this.props.onChange(value);
    }

    updateEntry(index: number, entryValue: *)
    {
        let value = [...this.props.value];
        value[index] = entryValue;
        this.props.onChange(value);
    }

    createOnClickRemoveEntry(index: number): (e: MouseEvent) => void
    {
        return (e: MouseEvent) => {
            e.preventDefault();
            this.removeEntry(index);
        }
    }

    render(): Node {
        return (
            <>
                { this.props.label && <h5>{this.props.label}</h5> }
                <a href="#" className="btn btn-secondary" onClick={this.onClickAddEntry.bind(this)}>{this.props.addItemLabel}</a>

                <div>
                    {this.props.value.map((entryValue: *, index: number) =>
                        <div key={`entry_${index}`} className="mt-2 d-flex align-items-center">
                            <a href="#"  onClick={this.createOnClickRemoveEntry(index)} className="btn btn-outline-secondary">×</a>
                            <div className="ps-2">
                                {React.createElement(this.props.entryType, {
                                    ...this.props.entryProps,
                                    value: entryValue,
                                    onChange: ((entryValue: *) => {
                                        this.updateEntry(index, entryValue);
                                    })
                                })}
                            </div>
                        </div>
                    )}
                </div>

            </>
        )
    }

}