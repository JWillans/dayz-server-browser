// @flow

/**
 * React components registered here can be added to a page in Twig
 *
 * Example:
 *
 *     {{ react_component('example', {'title': 'Hello world!'}) }}
 */


export function resolveComponent(name: string): Promise
{
    //if(name === 'ServerSearch') return import('./component/ServerSearch');
    if(name === 'App') return import('./component/App');

    throw 'Could not resolve React component "' + name + '"';
}
