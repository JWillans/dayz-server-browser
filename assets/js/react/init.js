// @flow

let containers = [];

type ComponentResolver = (name: string) => *;

let listeners = {
    'initContainer': []
}

type InitContainerCallback = (name: string) => void;

export function onInitContainer(callback: InitContainerCallback): void
{
    listeners.initContainer.push(callback);
}

async function initContainer(container: HTMLElement, resolveComponent: ComponentResolver): Promise<HTMLElement>
{
    if(containers.indexOf(container) > -1) return container;
    if(!container.hasAttribute('data-react')) return container;

    const name = container.getAttribute('data-react');
    if(name === null || name === undefined) throw 'Cannot initialise React component, attribute data-react is undefined or null';

    const React = await import('react');
    const ReactDOMClient = await import('react-dom/client');

    const defaults = {
        __innerHtml: container.innerHTML
    };

    const propsJson = container.getAttribute('data-react-props') ?? null;
    const props = {
        ...defaults,
        ...(propsJson ? JSON.parse(propsJson) : {})
    };

    const component = await resolveComponent(name);

    const element = React.createElement(component.default, props);
    const root = ReactDOMClient.createRoot(container);
    root.render(element);
    containers.push(container);

    listeners.initContainer.forEach((callback: InitContainerCallback) => {
        callback(name);
    })

    return container;
}

export function getContainers(): HTMLElement[]
{
    return Array.from(document.querySelectorAll('[data-react]'));
}

export async function initAll(resolveComponent: ComponentResolver): Promise<HTMLElement[]>
{
    return Promise.all(getContainers().map((container: HTMLElement) => {
        return initContainer(container, resolveComponent);
    }));
}

