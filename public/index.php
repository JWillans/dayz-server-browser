<?php

namespace App;

require_once __DIR__ . '/../vendor/autoload.php';

$kernel = new Kernel();

$response = $kernel->createPageGenerator()->generateResponse();
$response->send();