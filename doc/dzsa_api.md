# DayZ SA Launcher API

## Get server list

`GET https://dayzsalauncher.com/api/v1`

## Query server

`GET https://dayzsalauncher.com/api/v1/query/{address}/{port}`