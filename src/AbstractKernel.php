<?php

namespace App;

use Symfony\Component\Dotenv\Dotenv;

abstract class AbstractKernel
{

    /**
     * @var array<string, string>
     */
    protected array $envVars;

    public function __construct(?array $envVars = null)
    {
        $dotEnv = new Dotenv();
        $dotEnv->loadEnv(__DIR__ . '/../.env');

        $this->envVars = array_merge(
            $_SERVER,
            $envVars ?? [],
        );
    }

    public function getEnvVars(): array
    {
        return $this->envVars;
    }

    public function getEnvVar(string $key, string $default = ''): string
    {
        return $this->envVars[$key] ?? $default;
    }

    public function getEnvironment(): string
    {
        return $this->getEnvVar('APP_ENV', 'prod');
    }

}