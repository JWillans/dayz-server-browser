<?php

namespace App;

use App\Neocities\NeocitiesManager;
use App\Page\PageGenerator;
use Symfony\Component\Dotenv\Dotenv;

class Kernel extends AbstractKernel
{

    public function createPageGenerator(): PageGenerator
    {
        return new PageGenerator(
            $this->getEnvironment(),
        );
    }

    public function createNeocitiesManager(): NeocitiesManager
    {
        return new NeocitiesManager(
            $this->getEnvVar('NEOCITIES_USERNAME'),
            $this->getEnvVar('NEOCITIES_PASSWORD')
        );
    }

}