<?php

namespace App\Neocities;

use ReedJones\Neocities\Neocities;
use Symfony\Component\Finder\Finder;

class NeocitiesManager
{

    const KEEP_REMOTE_PATHS = [
        'not_found.html'
    ];

    protected Neocities $client;

    public function __construct(string $username, string $password)
    {
        $this->client = new Neocities([
            'username' => $username,
            'password' => $password,
        ]);
    }

    protected function getPublicDir(): string
    {
        $dir = __DIR__ . '/../../public';
        $realDir = realpath($dir);
        if($realDir === false) throw new \Exception(sprintf('Could not resolve real path for %s', $dir));
        return $realDir;
    }

    public function listRemoteFiles(): array
    {
        $entries = $this->client->list();

        $paths = array_map(function(object $entry){
            if($entry->is_directory) return null;
            return $entry->path;
        }, $entries->files);

        $paths = array_filter($paths);
        return array_values($paths);
    }

    protected function getPublicDirRegex(): string
    {
        return sprintf('#^%s/#', preg_quote($this->getPublicDir(), '#'));
    }

    public function listRemoteJunkFiles(): array
    {
        $local = $this->listLocalFiles();

        $publicDirRegex = $this->getPublicDirRegex();
        $localRel = array_map(function(string $path) use ($publicDirRegex){
            return preg_replace($publicDirRegex, '', $path);
        }, $local);

        $remote = $this->listRemoteFiles();

        return array_filter($remote, function(string $path) use ($localRel){
            if(in_array($path, self::KEEP_REMOTE_PATHS)) return false;
            return !in_array($path, $localRel);
        });
    }

    public function deleteRemoteJunkFiles(): void
    {
        $paths = $this->listRemoteJunkFiles();
        if(count($paths) === 0) return;
        $this->client->delete($paths);
    }

    public function listLocalFiles(): array
    {
        $finder = Finder::create()->files()
            ->in($this->getPublicDir())
            ->name('/\.(js|css|html|json)$/')
            ->exclude([
                'build/dev',
            ])
        ;

        $paths = [];

        foreach($finder as $file){
            $paths[] = $file->getRealPath();
        }

        return $paths;
    }

    public function resolveLocalRelPath(string $path): string
    {
        $regex = $this->getPublicDirRegex();
        $result = preg_replace($regex, '', $path);
        if(!is_string($result)) throw new \Exception(sprintf('Could not resolve local rel path for %s', $path));
        return $result;
    }

    public function uploadLocalFiles(): void
    {
        $paths = $this->listLocalFiles();

        $uploads = [];

        foreach($paths as $path){
            $relPath = $this->resolveLocalRelPath($path);
            $uploads[$relPath] = $path;
        }

        $this->client->upload($uploads);
    }

}