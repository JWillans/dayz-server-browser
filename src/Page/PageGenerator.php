<?php

namespace App\Page;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Response;

class PageGenerator
{

    public function __construct(
        protected string $env,
    )
    {}

    /**
     * @return array<string, string>
     */
    protected function generateContext(): array
    {
        return [
            'env' => $this->env,
            'js_entry_tags' => $this->generateEntryTagsFragment('js'),
            'css_entry_tags' => $this->generateEntryTagsFragment('css'),
        ];
    }

    protected function generateEntryTagsFragment(string $type): string
    {
        $entrypoints = $this->getEntryPoints();

        $tags = [];

        if($type === 'js'){
            foreach($entrypoints['entrypoints']['app']['js'] as $path){
                $tags[] = sprintf('<script src="%s"></script>', $path);
            }
        }
        elseif($type === 'css'){
            foreach($entrypoints['entrypoints']['app']['css'] as $path){
                $tags[] = sprintf('<link href="%s" rel="stylesheet">', $path);
            }
        }
        else{
            throw new \Exception(sprintf('Unsupported type %s', $type));
        }

        return implode("\n", $tags);
    }

    protected function getPublicDir(): string
    {
        return __DIR__ . '/../../public';
    }

    protected function getBuildDir(): string
    {
        return sprintf('%s/build/%s', $this->getPublicDir(), $this->env);
    }

    /**
     * @param string $path
     * @return array<string, mixed>
     * @throws \JsonException
     */
    protected function loadJson(string $path): array
    {
        $json = file_get_contents($path);
        if($json === false) throw new \Exception(sprintf('Could not read %s', $path));
        return json_decode($json, true, 512, JSON_THROW_ON_ERROR);
    }

    /**
     * @return array<string, mixed>
     * @throws \JsonException
     */
    protected function getEntryPoints(): array
    {
        $path = sprintf('%s/entrypoints.json', $this->getBuildDir());
        return $this->loadJson($path);
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function generateContent(): string
    {
        $fs = new Filesystem();
        $path = __DIR__ . '/../../templates/index.html';
        $html = file_get_contents($path);
        if($html === false) throw new \Exception(sprintf('Could not read %s', $path));

        $context = $this->generateContext();

        foreach($context as $key => $value){
            $html = str_replace(sprintf('{{ %s }}', $key), $value, $html);
        }

        return $html;
    }

    /**
     * @param string $path
     * @return void
     * @throws \Exception
     */
    public function dumpHtml(string $path): void
    {
        $fs = new Filesystem();

        $dir = dirname($path);
        if(!$fs->exists($dir)){
            $fs->mkdir($dir);
        }

        $html = $this->generateContent();
        $fs->dumpFile($path, $html);
    }

    /**
     * @return Response
     * @throws \Exception
     */
    public function generateResponse(): Response
    {
        return new Response($this->generateContent());
    }

}